// init user singleton
class UI {
    constructor() {
        this.login = document.querySelector('.login');
        this.authorized = document.querySelector('.authorized');
        this.roomsList = document.querySelector('.rooms-list');
        this.usersList = document.querySelector('.users-list');
        this.messageContainer = document.querySelector('.message-container');
        this.navUserName = document.querySelector('.user-name');
        this.user = USER.getInstance();
    }

    showLogin() {

    }

    hideLogin() {
        this.login.style.display = 'none';
    }

    showAuthorized() {
        let currentUser = this.user.getUser();
        this.navUserName.innerText = `${currentUser.username}`;
        this.authorized.style.display = 'block';
    }

    hideAuthorized() {

    }

    generateRooms(rooms) {
        this.roomsList.innerHTML = '';

        rooms.forEach((room, index) => this.roomsList.insertAdjacentHTML("beforeend", UI.roomListTemplate(room, index)));
    }

    generateUsersInRoom(users) {
        this.usersList.innerHTML = '';

        users.forEach(user => this.usersList.insertAdjacentHTML("beforeend", UI.userListTemplate(user)));
    }

    addMessage(info) {
        let className = this.getClass(info);
        this.messageContainer.insertAdjacentHTML("beforeend", UI.messageTemplate(info, className));
    }

    newUserJoin(name) {
        this.messageContainer.innerHTML = '';
        this.messageContainer.insertAdjacentHTML("beforeend", UI.newUserJoinTemplate(name));
    }

    userLeft(user) {
        this.messageContainer.innerHTML = '';
        this.messageContainer.insertAdjacentHTML("beforeend", UI.userLeftTemplate(user));
    }

    getClass(data) {
        let currentUser = this.user.getUser();

        if (data.username === currentUser.username) {
            return {
                target: 'from',
                bgColor: 'teal accent-2'
            }
        } else {
            return {
                target: 'to',
                bgColor: 'blue accent-2'
            }
        }
    }

    static roomListTemplate(room, index) {
        return `<li><a href="#" class="waves-effect" data-room-index="${index}">${room}</a></li>`;
    }

    static userListTemplate({name, id}) {
        return `<li class="collection-item" data-user-id="${id}">${name}</li>`;
    }

    static messageTemplate(msg, className) {
        return `
            <div class="message ${className.target} ">
                <div class="card ${className.bgColor}">
                    <div class="card-content">
                        <p class="black-text">${msg.username} says:</p>
                        <h6>${msg.message}</h6>
                    </div>
                </div>
           </div>
        `;
    }

    static newUserJoinTemplate(name) {
        return `
            <div class="card teal lighten-2">
                <div class="card-content">
                    <p>New user: ${name} joined chat</p>
                </div>
           </div>
        `;
    }

    static userLeftTemplate(user) {
        return `
            <div class="card teal lighten-2">
                <div class="card-content">
                    <p>${user} has left the room</p>
                </div>
           </div>
        `;
    }
}
